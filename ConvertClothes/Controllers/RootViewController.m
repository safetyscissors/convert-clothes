//
//  RootViewController.m
//  ConvertClothes
//
//  Created by Jason Lagaac on 2/19/13.
//  Copyright (c) 2013 Jason Lagaac. All rights reserved.
//

#import "RootViewController.h"
#import "MainViewController.h"

@interface RootViewController ()
- (void)loadSplashImage;
- (void)dismissSplashImage;
@end

@implementation RootViewController


- (void)viewDidLoad
{
    [super viewDidLoad];

    mainViewController = [[MainViewController alloc] init];
    [self loadSplashImage];
}

- (void)loadSplashImage
{
    splashImage = [[UIImageView alloc] init];
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    screenBounds = CGRectMake(screenBounds.origin.x, screenBounds.origin.y - 20.0f, screenBounds.size.width, screenBounds.size
                              .height);
    [splashImage setFrame:screenBounds];
    [splashImage setContentMode:UIViewContentModeScaleAspectFit];
    
    if (IS_IPHONE_5) {
        [splashImage setImage:[UIImage imageNamed:@"Default-568h@2x.png"]];
    } else {
        [splashImage setImage:[UIImage imageNamed:@"Default.png"]];
    }
    
    [self.view addSubview:splashImage];
    
    [NSTimer scheduledTimerWithTimeInterval:0.5
                                     target:self
                                   selector:@selector(dismissSplashImage)
                                   userInfo:nil
                                    repeats:NO];
}

- (void)dismissSplashImage {
    [UIView animateWithDuration:0.5 animations:^{
        [splashImage setAlpha:0.0];
    } completion:^(BOOL finished) {
        [splashImage removeFromSuperview];
        [self.view addSubview:mainViewController.view];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
