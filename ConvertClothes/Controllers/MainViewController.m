//
//  MainViewController.m
//  ConvertClothes
//
//  Created by Jason Lagaac on 2/20/13.
//  Copyright (c) 2013 Jason Lagaac. All rights reserved.
//

#import "MainViewController.h"

@interface MainViewController ()
- (void)imAFieldLblInit;
- (void)whoWearsFieldLblInit;
- (void)inFieldLblInit;
- (void)sizeFieldLblInit;
@end

@implementation MainViewController

- (id)init
{
    if (self = [super init]) {
        self.view = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [self imAFieldInit];
        [self whoWearsFieldInit];
        [self inFieldInit];
        [self sizeFieldInit];
        [self loadActionBar];
        [self loadSelectionBars];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

- (void)viewDidAppear:(BOOL)animated {
    [UIView animateWithDuration:0.3f animations:^{
        [imAFieldLbl setAlpha:1.0f];
        [genderLbl setAlpha:1.0f];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3f animations:^{
            [whoWearsFieldLbl setAlpha:1.0f];
            [clothingLbl setAlpha:1.0f];
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.3f animations:^{
                [inFieldLbl setAlpha:1.0f];
                [unitLbl setAlpha:1.0f];
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3f animations:^{
                    [sizeFieldLbl setAlpha:1.0f];
                    [sizeLbl setAlpha:1.0f];
                    
                    
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.3f animations:^{
                        CGPoint actionBarCenter = [actionBar center];
                        [actionBar setCenter:CGPointMake(actionBarCenter.x, actionBarCenter.y - 195.0f)];
                    }];
                }];
            }];
        }];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)layoutLabels
{
    
}

- (void)imAFieldInit
{
    imAFieldLbl = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(10.0f, 55.0f, 130.0f, 60.0f)];
    [imAFieldLbl setFont:[UIFont fontWithName:@"Roboto-Condensed" size:50]];
    [imAFieldLbl setText:@"I'm a"];
    [imAFieldLbl setTextAlignment:NSTextAlignmentRight];
    [imAFieldLbl setLineBreakMode:NSLineBreakByWordWrapping];
    [imAFieldLbl setTextColor:[UIColor colorWithRed:175.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0f]];
    [imAFieldLbl setNumberOfLines:0];
    [imAFieldLbl setAlpha:0.0f];
    [[self view] addSubview:imAFieldLbl];
    
    genderLbl = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(150.0f, 60.0f, 210.0f, 60.0f)];
    [genderLbl setFont:[UIFont fontWithName:@"Roboto-Condensed" size:45]];
    [genderLbl setText:@"Woman"];
    [genderLbl setLineBreakMode:NSLineBreakByWordWrapping];
    [genderLbl setTextColor:[UIColor colorWithRed:194.0f/255.0f green:56.0f/255.0f blue:73.0f/255.0f alpha:1.0f]];
    [genderLbl setNumberOfLines:0];
    [genderLbl setAlpha:0.0f];
    [[self view] addSubview:genderLbl];
}

- (void)whoWearsFieldInit
{
    whoWearsFieldLbl = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(20.0f, 145.0f, 120.0f, 130.0f)];
    [whoWearsFieldLbl setText:@"who\n wears"];

    [whoWearsFieldLbl setFont:[UIFont fontWithName:@"Roboto-Condensed" size:30]];
    [whoWearsFieldLbl setTextAlignment:NSTextAlignmentRight];
    [whoWearsFieldLbl setLineBreakMode:NSLineBreakByWordWrapping];
    [whoWearsFieldLbl setTextColor:[UIColor colorWithRed:175.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0f]];
    [whoWearsFieldLbl setNumberOfLines:0];
    [whoWearsFieldLbl setAlpha:0.0f];
    [[self view] addSubview:whoWearsFieldLbl];
    
    clothingLbl = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(150.0f, 148.0f, 200.0f, 150.0f)];
    [clothingLbl setText:@"Shoes"];
    [clothingLbl setFont:[UIFont fontWithName:@"Roboto-Condensed" size:60]];
    [clothingLbl setLineBreakMode:NSLineBreakByWordWrapping];
    [clothingLbl setTextColor:[UIColor colorWithRed:194.0f/255.0f green:56.0f/255.0f blue:73.0f/255.0f alpha:1.0f]];
    [clothingLbl setNumberOfLines:0];
    [clothingLbl setAlpha:0.0f];
    [[self view] addSubview:clothingLbl];
}

- (void)inFieldInit
{
    inFieldLbl = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(20.0f, 245.0f, 120.0f, 60.0f)];
    [inFieldLbl setText:@"in"];
    [inFieldLbl setFont:[UIFont fontWithName:@"Roboto-Condensed" size:50]];
    [inFieldLbl setTextAlignment:NSTextAlignmentRight];
    [inFieldLbl setLineBreakMode:NSLineBreakByWordWrapping];
    [inFieldLbl setTextColor:[UIColor colorWithRed:175.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0f]];
    [inFieldLbl setNumberOfLines:0];
    [inFieldLbl setAlpha:0.0f];
    [[self view] addSubview:inFieldLbl];
    
    unitLbl = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(150.0f, 235.0f, 200.0f, 150.0f)];
    [unitLbl setText:@"US"];
    [unitLbl setFont:[UIFont fontWithName:@"Roboto-Condensed" size:80]];
    [unitLbl setLineBreakMode:NSLineBreakByWordWrapping];
    [unitLbl setTextColor:[UIColor colorWithRed:194.0f/255.0f green:56.0f/255.0f blue:73.0f/255.0f alpha:1.0f]];
    [unitLbl setNumberOfLines:0];
    [unitLbl setAlpha:0.0f];
    [[self view] addSubview:unitLbl];
}

- (void)sizeFieldInit
{
    sizeFieldLbl = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(20.0f, 345.0f, 120.0f, 60.0f)];
    [sizeFieldLbl setText:@"size"];
    [sizeFieldLbl setFont:[UIFont fontWithName:@"Roboto-Condensed" size:50]];
    [sizeFieldLbl setTextAlignment:NSTextAlignmentRight];
    [sizeFieldLbl setLineBreakMode:NSLineBreakByWordWrapping];
    [sizeFieldLbl setTextColor:[UIColor colorWithRed:175.0f/255.0f green:169.0f/255.0f blue:169.0f/255.0f alpha:1.0f]];
    [sizeFieldLbl setNumberOfLines:0];
    [sizeFieldLbl setAlpha:0.0f];

    [sizeFieldLbl setAlpha:0.0f];
    [[self view] addSubview:sizeFieldLbl];
    
    sizeLbl = [[OHAttributedLabel alloc] initWithFrame:CGRectMake(150.0f, 335.0f, 200.0f, 150.0f)];
    [sizeLbl setText:@"28"];
    [sizeLbl setFont:[UIFont fontWithName:@"Roboto-Condensed" size:70]];
    [sizeLbl setLineBreakMode:NSLineBreakByWordWrapping];
    [sizeLbl setTextColor:[UIColor colorWithRed:194.0f/255.0f green:56.0f/255.0f blue:73.0f/255.0f alpha:1.0f]];
    [sizeLbl setNumberOfLines:0];
    [sizeLbl setAlpha:0.0f];
    [[self view] addSubview:sizeLbl];
}

- (void)loadActionBar
{
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    actionBar = [[UIView alloc] initWithFrame:CGRectMake(screenBounds.origin.x, screenBounds.size.height + 100.0f, screenBounds.size.width, 95.0f)];
    
    CGRect actionBarBounds = [actionBar bounds];
    CGRect leftButton = CGRectMake(0, actionBarBounds.origin.y, actionBarBounds.size.width / 2, actionBarBounds.size.height - 15.0f);
    CGRect rightButton = CGRectMake((actionBarBounds.size.width / 4) * 2, actionBarBounds.origin.y, actionBarBounds.size.width / 2, actionBarBounds.size.height - 15.0f);
    
    savedButton = [[UIButton alloc] initWithFrame:leftButton];
    [savedButton setTitle:@"SAVED" forState:UIControlStateNormal];
    [[savedButton titleLabel] setFont:[UIFont fontWithName:@"Roboto-Condensed" size:25]];
    [savedButton setBackgroundColor:[UIColor colorWithRed:209.0f/255.0f green:209.0f/255.0f blue:209.0f/255.0f alpha:1.0f]];

    convertButton = [[UIButton alloc] initWithFrame:rightButton];
    [convertButton setTitle:@"CONVERT" forState:UIControlStateNormal];
    [[convertButton titleLabel] setFont:[UIFont fontWithName:@"Roboto-Condensed" size:25]];
    [convertButton setBackgroundColor:[UIColor colorWithRed:51.0f/255.0f green:51.0f/255.0f blue:51.0f/255.0f alpha:1.0f]];
    
    [actionBar addSubview:convertButton];
    [actionBar addSubview:savedButton];

    [[self view] addSubview:actionBar];
}

- (void)loadSelectionBars
{
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    CGPoint selectionBarOrigin = CGPointMake(screenBounds.size.width, 0);
    CGRect selectionBarFrame = CGRectMake(selectionBarOrigin.x, selectionBarOrigin.y, screenBounds.size.width * 0.5f, screenBounds.size.height);
    
    selectionBar = [[UIView alloc] initWithFrame:selectionBarFrame];
    [selectionBar setBackgroundColor:[UIColor blackColor]];
    [[self view] addSubview:selectionBar];
    
    CGPoint indicatorBarOrigin = CGPointMake(-(screenBounds.size.width / 2), 0);
    CGRect indicatorBarFrame = CGRectMake(indicatorBarOrigin.x, indicatorBarOrigin.y, screenBounds.size.width * 0.5f, screenBounds.size.height);
    indicatorBar = [[UIView alloc] initWithFrame:indicatorBarFrame];
    [indicatorBar setBackgroundColor:[UIColor blueColor]];
    [[self view] addSubview:indicatorBar];
}

@end
