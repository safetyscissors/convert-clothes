//
//  MainViewController.h
//  ConvertClothes
//
//  Created by Jason Lagaac on 2/20/13.
//  Copyright (c) 2013 Jason Lagaac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <OHAttributedLabel/OHAttributedLabel.h>

@interface MainViewController : UIViewController
{
    OHAttributedLabel *imAFieldLbl;
    OHAttributedLabel *whoWearsFieldLbl;
    OHAttributedLabel *inFieldLbl;
    OHAttributedLabel *sizeFieldLbl;
    
    OHAttributedLabel *genderLbl;
    OHAttributedLabel *clothingLbl;
    OHAttributedLabel *unitLbl;
    OHAttributedLabel *sizeLbl;
    
    
    UIView *actionBar;
    UIButton *convertButton;
    UIButton *savedButton;
    
    UIView *selectionBar;
    UITableView *selectionTable;
    
    UIView *indicatorBar;
    OHAttributedLabel *indicatorField;
    OHAttributedLabel *indicatorValue;
}

@end
