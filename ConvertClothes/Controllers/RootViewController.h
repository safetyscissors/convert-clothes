//
//  RootViewController.h
//  ConvertClothes
//
//  Created by Jason Lagaac on 2/19/13.
//  Copyright (c) 2013 Jason Lagaac. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MainViewController;

@interface RootViewController : UIViewController
{
    MainViewController *mainViewController;
    UIImageView *splashImage;
}

@end
